# MVVM Module Generator

Template to generate MVVM modules.

## Installation
- [Download MVVM Template](https://bitbucket.org/lets_find_the_truth/mvvm-module-generator/get/05993535370a.zip) or clone the project
- Copy the `MVVM` folder to `~/Library/Developer/Xcode/Templates/File Templates/` or create a symbolic link to that folder.

## Using the template
- Start Xcode
- Create a new file (`File > New > File` or `⌘N`)
- Choose `MVVM` then either `MVVM Initial Module`  or ‘MVVM Module’

## Created Files
- FolderWithModuleName
- `ViewController`
- `ViewModel`
- `Coordinator`

