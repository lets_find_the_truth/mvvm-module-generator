//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import RxSwift
import RxCocoa

class ___VARIABLE_Name___ViewController: UIViewController {
    private let disposeBag = DisposeBag()
    var viewModel: ___VARIABLE_Name___ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBindings()
    }
    
    private func setupBindings() {
        
    }
}
