//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import RxSwift

enum ___VARIABLE_Name___CoordinationResult {
    case language(String)
    case cancel
} // or delete and put void

class ___VARIABLE_Name___Coordinator: BaseCoordinator<___VARIABLE_Name___CoordinationResult>{

    private let rootViewController: UIViewController
    
    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }
    
    override func start() -> Observable<CoordinationResult> {
        let viewController = UIStoryboard(name: "Main", bundle: .main).initFromStoryboard(viewController: ___VARIABLE_Name___ViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = ___VARIABLE_Name___ViewModel()
        viewController.viewModel = viewModel
        
        let cancel = viewModel.didCancel.map { _ in CoordinationResult.cancel }
        let language = viewModel.didSelectLanguage.map { CoordinationResult.language($0) }
        
        rootViewController.present(navigationController, animated: true)
        
        return Observable.merge(cancel, language)
            .take(1)
            .do(onNext: { [weak self] _ in self?.rootViewController.dismiss(animated: true) })
    }
}

