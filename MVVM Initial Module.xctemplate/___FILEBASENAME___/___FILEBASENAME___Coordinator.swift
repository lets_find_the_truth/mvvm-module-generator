//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import RxSwift

class ___VARIABLE_Name___Coordinator: BaseCoordinator<Void>{

    private let window: UIWindow
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        let viewController = UIStoryboard(name: "Main", bundle: .main).initFromStoryboard(viewController: ___VARIABLE_Name___ViewController.self)
        let navigationController = UINavigationController(rootViewController: viewController)
        
        let viewModel = ___VARIABLE_Name___ViewModel()
        viewController.viewModel = viewModel
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        return Observable.never()
    }
    
    private func showViewControllerOn(rootViewController: UIViewController) -> Observable<String?> {
        // CHANGE STANDART IMPLEMENTATION (other)
        let otherCoordinator = OtherViewControllerCoordinator(rootViewController: rootViewController)
        return coordinate(to: otherCoordinator)
            .map { result in
                switch result {
                case .value(let value): return value
                case .cancel: return nil
                }
        }
    }
}

